package com.orasi;

import static org.junit.Assert.*;
import java.util.Date;
import org.junit.Test;

public class SomeServletTest
{
	@Test
	public void testDate()
	{
		SomeServlet myServlet = new SomeServlet();
		assertNotNull("The date was null!", myServlet.getDate());
	}
	
	@Test
	public void testGetBuildNumber()
	{
		SomeServlet myServlet = new SomeServlet();
		assertNotNull("The build number was null!", myServlet.getBuildNumber());
	}
}

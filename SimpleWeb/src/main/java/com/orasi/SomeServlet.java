package com.orasi;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.InputStream;

import java.util.Date;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SomeServlet extends HttpServlet 
{

	private static final long serialVersionUID = 1L;
    public static final String HTML_START="<html><body>";
    public static final String HTML_END="</body></html>";
	private Properties props = new Properties();
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		Date date = this.getDate();
		int buildNum = this.getBuildNumber();
		
		resp.setContentType("text/html");

		PrintWriter out = resp.getWriter();
		out.println(HTML_START + "<h2>Hi There!</h2><br/><h3>This is from Jenkins build number " + buildNum + "</h3></br>" + HTML_END);
		out.println("<input type=\"button\" value=\"Get Current Time\" onClick=\"window.location.reload()\">");
		out.println("<br/><h3>" + date + "</h3>");
	}
	
	protected Date getDate()
	{
		Date date = new Date();
		return date;
	}

	protected int getBuildNumber()
	{
		this.loadProperties();
		int num = 999;
		if (props.getProperty("BUILD_NUMBER") != null)
			num = Integer.parseInt(props.getProperty("BUILD_NUMBER"));

		return num;
	}
	
	protected void loadProperties()
	{
		InputStream is = this.getClass().getClassLoader().getResourceAsStream("jenkins.properties");
		try
		{
			props.load(is);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally {
    		try {
        		is.close();
    		} 
			catch(IOException e) {
				e.printStackTrace();
    		}
		}
	}

}